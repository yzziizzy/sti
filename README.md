# sti
General extension library for C. No dependencies beyond common POSIX platform headers. "[st]andard library, [i]zzy", in the same vein as stb.

`#include "sti/sti.h"` and compile `sti/sti.c` to get everything at once, or select individual pairs a la carte.


(Un)licensed under the Unlicense, which is effectively lawyer speak for 
Public Domain in all sane countries that have the concept of Public Domain. 
To quote Sam Hocevar, You just DO WHAT THE FUCK YOU WANT TO.


## TODO
* Documentation
* sexp could use some love:
	* Escaped chars string support
	* More api helper fns
	* Named arguments
* Better hash table options or macros
* Parser state machine macros and generator from compiler project
* Basic tests for important fns
